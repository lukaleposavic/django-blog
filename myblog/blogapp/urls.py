from django.urls import path
#from . import views
from .views import HomeView, addPostView, articleDetailsView,updatePostView,deletePostView

urlpatterns = [
    #path('', views.home, name = "home"),
    path('',HomeView.as_view(),name = "home"),
    path('article/<int:pk>',articleDetailsView.as_view(),name = 'article-detail'),
    path('add_post',addPostView.as_view(), name = 'add-post'),
    path('update_post/<int:pk>',updatePostView.as_view(), name = 'update'),
    path('delete_post/<int:pk>',deletePostView.as_view(), name = 'delete'),
]

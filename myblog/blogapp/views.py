
from django.shortcuts import render
from django.views.generic import ListView, DetailView,CreateView,UpdateView,DeleteView
from .models import Post
from django.urls import reverse_lazy
# Create your views here.

#def home(request):
 #   return render(request,'home.html',{})


class HomeView(ListView):
    model = Post
    template_name = 'home.html'
    ordering = ['-id']



class articleDetailsView(DetailView):
    model = Post
    template_name = 'article_details.html'


class addPostView(CreateView):
    model = Post
    template_name = 'add_post.html'
    fields = '__all__'

class updatePostView(UpdateView):
    model = Post
    template_name = 'update_post.html'
    fields = '__all__'


class deletePostView(DeleteView):
    model = Post
    template_name = 'delete.html'
    success_url = reverse_lazy('home')
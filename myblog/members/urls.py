from django.urls import path
from .views import userCreationView

urlpatterns = [
    path('register/', userCreationView.as_view(), name = 'register')
    
]
